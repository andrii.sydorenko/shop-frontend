export interface IFeedback {
  id?: number;
  user_email: string;
  product_id: number;
  feedback_message: string;
  sending_date?: number;
}