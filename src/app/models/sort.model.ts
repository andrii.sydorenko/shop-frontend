export interface ISort {
  limit: number;
  offset: number;
  orderBy?: string;
  sortBy?: string;
}
