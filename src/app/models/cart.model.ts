export interface ICart {
  id: string;
  items: ICartItem[];
}

export interface ICartItem {
  id?: number;
  product_id: number;
  quantity: number;
  shopping_cart_id?: number;
}
