export interface IProduct {
  id?: number;
  title: string;
  description: string;
  price: number;
  picture_path?: string;
  viewed?: number;
  category_id?: number;
  creation_date?: number;
  quantity?: number;
  size?: number;
  cart_item_id?: number;
}
