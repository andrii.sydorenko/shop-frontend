export interface ISize {
  id: string;
  size: number;
}
