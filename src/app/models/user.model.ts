export interface IUser {
  id?: number;
  first_name: string;
  last_name: string;
  is_admin?: boolean;
  email: string;
  password?: string;
  registration_date?: number;
}
