import { TokenStorageService } from 'src/app/services/token-storage.service';
import { IProduct } from '../../models/product.model';
import { Component, Input, OnInit } from '@angular/core';
import { environment } from 'src/app/environment';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss'],
})
export class ProductCardComponent implements OnInit {
  @Input() product: IProduct;
  url = environment.EDITED_PICTURE_ENDPOINT;
  constructor(private tokenStorage: TokenStorageService) {}

  ngOnInit(): void {}

  onView() {
    this.tokenStorage.saveViewedItem(this.product.id);
  }
}
