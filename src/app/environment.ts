export const environment = {
  production: false,
  SERVER_ENDPOINT: 'http://dcodeit.net:20000/',
  PICTURE_ENDPOINT: 'http://dcodeit.net:20000/product-images/',
  EDITED_PICTURE_ENDPOINT: 'http://dcodeit.net:20000/product-images-edited/'
  // SERVER_ENDPOINT: 'http://localhost:20000/',
  // PICTURE_ENDPOINT: 'http://localhost:20000/product-images/',
  // EDITED_PICTURE_ENDPOINT: 'http://localhost:20000/product-images-edited/',
};
