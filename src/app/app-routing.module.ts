import { AuthGuardService } from './services/auth-guard.service';
import { CartComponent } from './components/client/cart/cart.component';
import { MainComponent } from './components/client/main/main.component';
import { ProductComponent } from './components/client/product/product.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductCategoryComponent } from './components/client/product-category/product-category.component';
import { SearchComponent } from './components/client/search/search.component';

const routes: Routes = [
  {
    path: 'home',
    component: MainComponent,
  },
  {
    path: 'category/:id',
    component: ProductCategoryComponent,
  },
  {
    path: 'product/:id',
    pathMatch: 'full',
    component: ProductComponent,
  },
  { path: 'cart', component: CartComponent },
  { path: 'search', component: SearchComponent, },
  {
    path: 'auth',
    loadChildren: () =>
      import('./components/auth/auth.module').then((m) => m.AuthModule),
  },
  {
    path: 'admin',
    loadChildren: () =>
      import('./components/admin/admin.module').then((m) => m.AdminModule),
    canActivate: [AuthGuardService],
  },
  { path: '', pathMatch: 'full', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
