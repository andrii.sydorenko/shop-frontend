import { HttpClient } from '@angular/common/http';
import { IProduct } from './../models/product.model';
import { from, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from '../environment';
import { ISize } from '../models/size.model';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  url = environment.SERVER_ENDPOINT;

  constructor(private http: HttpClient) {}

  getAllProducts(options): Observable<IProduct[]> {
    const { limit, offset, sortBy = 'id', orderBy = 'asc' } = options;

    return this.http.get<IProduct[]>(`${this.url}products`, {
      params: {
        limit,
        offset,
        sortBy,
        orderBy,
      },
    });
  }

  getProductsFilteredByPriceAndOrdered(
    priceRange,
    options,
    categoryId: number
  ): Observable<IProduct[]> {
    const { min, max } = priceRange;

    const {
      orderBy = 'asc',
      sortBy = 'id',
      limit = '10',
      offset = '0',
    } = options;

    return this.http.get<IProduct[]>(
      this.url + 'products/category/' + categoryId,
      {
        params: {
          limit,
          offset,
          maxPrice: max,
          minPrice: min,
          orderBy,
          sortBy,
        },
      }
    );
  }

  getMostRecentAddedProducts(): Observable<IProduct[]> {
    return this.http.get<IProduct[]>(`${this.url}products`, {
      params: {
        limit: '6',
        offset: '0',
        orderBy: 'desc',
        sortBy: 'creation_date',
      },
    });
  }

  getMostPopularProducts(): Observable<IProduct[]> {
    return this.http.get<IProduct[]>(`${this.url}products`, {
      params: {
        limit: '6',
        offset: '0',
        orderBy: 'desc',
        sortBy: 'viewed',
      },
    });
  }

  getFilteredProducts(filter: string, options): Observable<IProduct[]> {
    const { limit, offset } = options;

    return this.http.get<IProduct[]>(`${this.url}products`, {
      params: {
        filter,
        limit,
        offset,
      },
    });
  }

  getRecomendedProducts(): Observable<IProduct[]> {
    return this.http.get<IProduct[]>(this.url + 'recommended-products');
  }

  addRecommendedProductById(productId: number): Observable<IProduct> {
    return this.http.post<IProduct>(`${this.url}recommended-products`, {
      productId,
    });
  }

  deleteRecommendedProduct(id: number): Observable<void> {
    return this.http.delete<void>(`${this.url}recommended-products/${id}`);
  }

  getRecentlyViewedProducts(): Observable<IProduct[]> {
    return this.http.get<IProduct[]>(`${this.url}products`);
  }

  // getRecentlyAddedProducts(): Observable<IProduct[]> {
  //   return this.http.get<IProduct[]>(`${this.url}products`);
  // }

  getPopularProducts(): Observable<IProduct[]> {
    return this.http.get<IProduct[]>(`${this.url}products`);
  }

  getProduct(id: number): Observable<IProduct> {
    return this.http.get<IProduct>(this.url + 'products/' + id);
  }

  getProductsByCategory(id: number, options): Observable<any> {
    return this.http.get<any>(this.url + 'products/category/' + id, {
      params: {
        ...options,
      },
    });
  }

  getSizesByTitle(title: string): Observable<ISize[]> {
    return this.http.get<ISize[]>(`${this.url}products/sizes/${title}`);
  }

  createProduct(product: IProduct): Observable<IProduct> {
    console.log(product);
    
    return this.http.post<IProduct>(`${this.url}products`, product);
  }

  sendProductPicture(picture: FormData): Observable<{}> {
    return this.http.post(`${this.url}products/picture`, picture);
  }

  updateProduct(id: number, product: IProduct): Observable<IProduct> {
    return this.http.put<IProduct>(`${this.url}products/${id}`, product);
  }

  deleteProduct(id: number): Observable<void> {
    return this.http.delete<void>(`${this.url}products/${id}`);
  }
}
