import { HttpClient } from '@angular/common/http';
import { IUser } from 'src/app/models/user.model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../environment';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  url = environment.SERVER_ENDPOINT;

  constructor(private http: HttpClient) {}

  getUsers(options): Observable<IUser[]> {
    const {
      orderBy = 'asc',
      sortBy = 'id',
      limit = '10',
      offset = '0',
    } = options;

    return this.http.get<IUser[]>(`${this.url}users`, {
      params: {
        limit,
        offset,
        orderBy,
        sortBy,
      },
    });
  }

  createUser(user: IUser): Observable<IUser> {
    return this.http.post<IUser>(`${this.url}users`, user);
  }

  updateUser(user: IUser): Observable<IUser> {
    const { id } = user;
    return this.http.put<IUser>(`${this.url}users/${id}`, user);
  }

  deleteUser(id: number): Observable<void> {
    return this.http.delete<void>(`${this.url}users/${id}`);
  }
}
