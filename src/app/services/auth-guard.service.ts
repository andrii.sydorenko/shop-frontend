import { TokenStorageService } from 'src/app/services/token-storage.service';
import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private tokenStorage: TokenStorageService) {}
  canActivate(): boolean {
    return this.tokenStorage.getAdmin();
  }
}
