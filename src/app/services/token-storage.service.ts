import { IProduct } from './../models/product.model';
import { ICartItem } from './../models/cart.model';
import { Injectable } from '@angular/core';

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';
const CART_KEY = 'cart-id';
const ADMIN_KEY = 'admin';

const CART_ITEMS_KEY = 'cart-items';
const VIEWED_ITEMS_KEY = 'viewed-items';

@Injectable({
  providedIn: 'root',
})
export class TokenStorageService {
  constructor() {}

  signOut(): void {
    window.localStorage.removeItem(USER_KEY);
    window.localStorage.removeItem(CART_KEY);
    window.localStorage.removeItem(TOKEN_KEY);
    if (this.getAdmin) {
      window.localStorage.removeItem(ADMIN_KEY);
    }
  }

  public saveToken(token: string): void {
    window.localStorage.removeItem(TOKEN_KEY);
    window.localStorage.setItem(TOKEN_KEY, token);
  }

  public getToken(): string {
    return localStorage.getItem(TOKEN_KEY);
  }

  public saveShoppingCart(shoppingCartId: string): void {
    window.localStorage.removeItem(CART_KEY);
    window.localStorage.setItem(CART_KEY, shoppingCartId);
  }

  public getShoppingCart(): string {
    return localStorage.getItem(CART_KEY);
  }

  public saveUser(user): void {
    window.localStorage.removeItem(USER_KEY);
    window.localStorage.setItem(USER_KEY, JSON.stringify(user));
  }

  public getUser(): any {
    return JSON.parse(localStorage.getItem(USER_KEY));
  }

  //ADMIN
  public saveAdmin(isAdmin): void {
    if (!isAdmin) isAdmin = false;
    window.localStorage.removeItem(ADMIN_KEY);
    window.localStorage.setItem(ADMIN_KEY, JSON.stringify(isAdmin));
  }

  public getAdmin(): any {
    return JSON.parse(localStorage.getItem(ADMIN_KEY));
  }

  //CART ITEMS
  public saveCartItem(cartItem: ICartItem): void {
    let productsString = localStorage.getItem(CART_ITEMS_KEY);
    let products = [];
    if (productsString) {
      products = JSON.parse(productsString);
    }
    products = [...products, cartItem];

    localStorage.setItem(CART_ITEMS_KEY, JSON.stringify(products));
  }

  public getCartItems(): ICartItem[] {
    return JSON.parse(localStorage.getItem(CART_ITEMS_KEY)) || [];
  }

  public removeCartItem(productId: number) {
    let productsString = localStorage.getItem(CART_ITEMS_KEY);
    let products = [];
    if (productsString) {
      products = JSON.parse(productsString);
    }
    products = products.filter(
      (product, idx) => product.product_id !== productId
    );
    localStorage.setItem(CART_ITEMS_KEY, JSON.stringify(products));
  }

  public changeQuantity(productId: number, quantity: number) {
    let productsString = localStorage.getItem(CART_ITEMS_KEY);
    let products = [];

    if (productsString) {
      products = JSON.parse(productsString);
    }

    products.forEach((product) => {
      if (product.product_id === productId) {
        product.quantity = quantity;
      }
    });

    localStorage.setItem(CART_ITEMS_KEY, JSON.stringify(products));
  }

  public clearCart(): any {
    localStorage.removeItem(CART_ITEMS_KEY);
  }

  public isProductAdded(productId: number): boolean {
    const products = JSON.parse(localStorage.getItem(CART_ITEMS_KEY));

    if (products) {
      for (let product of products) {
        if (product.product_id === productId) {
          return true;
        }
      }
    }

    return false;
  }

  // Viewed products
  public saveViewedItem(id: number): void {
    let productsString = localStorage.getItem(VIEWED_ITEMS_KEY);
    let products = [];
    if (productsString) {
      products = JSON.parse(productsString);
    }

    if (!products.find((productId) => productId === id)) {
      products = [id, ...products];
    }

    if (products.length > 5) {
      products = products.slice(0, 5);
    }

    localStorage.setItem(VIEWED_ITEMS_KEY, JSON.stringify(products));
  }

  public getViewedItems(): number[] {
    return JSON.parse(localStorage.getItem(VIEWED_ITEMS_KEY)) || [];
  }
}
