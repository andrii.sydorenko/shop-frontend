import { TokenStorageService } from './token-storage.service';
import { ICartItem } from './../models/cart.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../environment';

@Injectable({
  providedIn: 'root',
})
export class CartService {
  url = environment.SERVER_ENDPOINT;
  userEmail: string;
  cartId: number;

  constructor(
    private http: HttpClient,
    private tokenStorage: TokenStorageService
  ) {
    this.userEmail = this.tokenStorage.getUser();
    this.cartId = +this.tokenStorage.getShoppingCart();
  }

  async syncData(cartId: number): Promise<void> {
    this.cartId = cartId;
    const cartItems = await this.tokenStorage.getCartItems();

    for (let cartItem of cartItems) {
      this.postCartItem(cartItem).subscribe();
    }

    await this.tokenStorage.clearCart();

    await this.getCartItems(cartId).subscribe((cartItems) => {
      for (let cartItem of cartItems) {
        this.tokenStorage.saveCartItem(cartItem);
      }
    });
  }

  getCartItems(cartId: number): Observable<ICartItem[]> {
    return this.http.get<ICartItem[]>(`${this.url}cart-items/cart/${cartId}`);
  }

  getTotal(): Observable<{}> {
    return this.http.get<{}>(`${this.url}cart-items/total/cart/${this.cartId}`);
  }

  postCartItem(cartItem: ICartItem): Observable<ICartItem> {
    cartItem = { ...cartItem, shopping_cart_id: this.cartId };

    return this.http.post<ICartItem>(`${this.url}cart-items`, cartItem);
  }

  updateCartItem(cartItem: ICartItem): Observable<ICartItem> {
    cartItem = { ...cartItem, shopping_cart_id: this.cartId };

    return this.http.put<ICartItem>(
      `${this.url}cart-items/${cartItem.product_id}`,
      cartItem
    );
  }

  removeCartItem(cartItemId: number): Observable<void> {
    return this.http.delete<void>(`${this.url}cart-items/${cartItemId}`);
  }

  clearCart(): Observable<void> {
    return this.http.delete<void>(`${this.url}cart-items/cart/${this.cartId}`);
  }
}
