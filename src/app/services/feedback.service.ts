import { IFeedback } from './../models/feedback.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../environment';

@Injectable({
  providedIn: 'root',
})
export class FeedbackService {
  url = environment.SERVER_ENDPOINT + 'feedbacks/';

  constructor(private http: HttpClient) {}

  getAllFeedbacksByProduct(productId: number): Observable<IFeedback[]> {
    return this.http.get<IFeedback[]>(`${this.url}product/${productId}`);
  }

  createFeedback(feedback: IFeedback) {
    return this.http.post<IFeedback>(`${this.url}`, feedback);
  }
}
