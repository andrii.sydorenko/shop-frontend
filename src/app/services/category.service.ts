import { ICategory } from './../models/category.model';
import { environment } from '../environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  url = environment.SERVER_ENDPOINT + 'categories';

  constructor(private http: HttpClient) {}

  getCategories(options): Observable<ICategory[]> {
    const {
      limit = '10',
      offset = '0',
      sortBy = 'id',
      orderBy = 'asc',
    } = options;

    return this.http.get<ICategory[]>(this.url, {
      params: {
        orderBy,
        sortBy,
        limit,
        offset,
      },
    });
  }

  getCategoryById(id: number): Observable<ICategory> {
    return this.http.get<ICategory>(`${this.url}/${id}`);
  }

  createCategory(category: ICategory): Observable<ICategory> {
    return this.http.post<ICategory>(this.url, category);
  }

  updateCategory(id, categoryName): Observable<ICategory> {
    return this.http.put<ICategory>(`${this.url}/${id}`, { categoryName });
  }

  deleteCategory(id): Observable<{}> {
    return this.http.delete(`${this.url}/${id}`);
  }
}
