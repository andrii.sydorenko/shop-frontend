import { ProductService } from './../../services/product.service';
import { IProduct } from './../../models/product.model';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/app/environment';

@Component({
  selector: 'app-recently-added-products',
  templateUrl: './recently-added-products.component.html',
  styleUrls: ['./recently-added-products.component.scss'],
})
export class RecentlyAddedProductsComponent implements OnInit {
  products: IProduct[] = [];
  url = environment.SERVER_ENDPOINT + 'product-images-edited/';

  constructor(private productService: ProductService) {}

  ngOnInit(): void {
    this.productService.getMostRecentAddedProducts().subscribe((products) => {
      this.products = products['products'];
    });
  }
}
