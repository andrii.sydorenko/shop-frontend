import { TokenStorageService } from 'src/app/services/token-storage.service';
import { ProductService } from './../../services/product.service';
import { IProduct } from './../../models/product.model';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/app/environment';

@Component({
  selector: 'app-recently-viewed-products',
  templateUrl: './recently-viewed-products.component.html',
  styleUrls: ['./recently-viewed-products.component.scss'],
})
export class RecentlyViewedProductsComponent implements OnInit {
  products: IProduct[] = [];
  url = environment.EDITED_PICTURE_ENDPOINT;

  constructor(
    private tokenStorage: TokenStorageService,
    private productService: ProductService
  ) {}

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts() {
    const ids = this.tokenStorage.getViewedItems();
    const products: IProduct[] = [];

    for (let id of ids) {
      this.productService.getProduct(id).subscribe((product) => {
        if (product) {
          products.push(product);
        }
      });
    }
    this.products = products;
  }
}
