import { ProductService } from './../../services/product.service';
import { IProduct } from './../../models/product.model';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/app/environment';

@Component({
  selector: 'app-recomended-products',
  templateUrl: './recomended-products.component.html',
  styleUrls: ['./recomended-products.component.scss'],
})
export class RecomendedProductsComponent implements OnInit {
  products: IProduct[] = [];
  url = environment.SERVER_ENDPOINT + 'product-images-edited/';

  constructor(private productService: ProductService) {}

  ngOnInit(): void {
    this.productService.getRecomendedProducts().subscribe((products) => {
      this.products = products;
    });
  }
}
