import { ProductService } from './../../services/product.service';
import { IProduct } from './../../models/product.model';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/app/environment';

@Component({
  selector: 'app-popular-products',
  templateUrl: './popular-products.component.html',
  styleUrls: ['./popular-products.component.scss'],
})
export class PopularProductsComponent implements OnInit {
  products: IProduct[] = [];
  url = environment.SERVER_ENDPOINT + 'product-images-edited/'

  constructor(private productService: ProductService) {}

  ngOnInit(): void {
    this.productService.getMostPopularProducts().subscribe((products) => {
      this.products = products['products'];
    });
  }
}
