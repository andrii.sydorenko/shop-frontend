import { TokenStorageService } from 'src/app/services/token-storage.service';
import { IProduct } from './../../models/product.model';
import { ProductService } from './../../services/product.service';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/app/environment';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss'],
})
export class BannerComponent implements OnInit {
  product: IProduct;
  isLoaded: boolean = false;
  url = environment.PICTURE_ENDPOINT;

  constructor(
    private productService: ProductService,
    private tokenStorage: TokenStorageService
  ) {}

  ngOnInit(): void {
    this.productService.getMostPopularProducts().subscribe((products) => {
      products = products['products'];
      const rand = Math.floor(Math.random() * products.length);
      if (products[rand]) {
        this.product = products[rand];
        this.isLoaded = true;
      }
    });
  }

  onView() {
    this.tokenStorage.saveViewedItem(this.product.id);
  }
}
