import { FormControl } from '@angular/forms';

export class CustomValidators {
  public static noWhitespaceValidator(control: FormControl) {
    if (control.value == '') return null;

    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { whitespace: true };
  }

  public static email(control: FormControl) {
    if (control.value == '') return null;

    const pattern = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
    const isEmail = pattern.test(control.value || '');
    const isValid = isEmail;
    return isValid ? null : { email: true };
  }
}
