import { Sort } from '@angular/material/sort';
import { FormControl } from '@angular/forms';
import { DeleteUserDialogComponent } from './delete-user/delete-user.component';
import { AddUserDialogComponent } from './add-user/add-user.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { IUser } from 'src/app/models/user.model';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  isLoaded: boolean = false;

  public pageSize = 10;
  public currentPage = 0;
  public totalSize = 0;
  private sortOptions: Sort = { active: 'id', direction: 'asc' };

  displayedColumns: string[] = [
    'id',
    'firstName',
    'lastName',
    'email',
    'isAdmin',
    'registrationDate',
    'actions',
  ];
  dataSource: MatTableDataSource<IUser>;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private usersService: UsersService, public dialog: MatDialog) {}

  ngOnInit() {
    this.usersService
      .getUsers({
        limit: this.pageSize,
        offset: this.currentPage * this.pageSize,
      })
      .subscribe((users) => {
        this.dataSource = new MatTableDataSource(users['users']);
        this.totalSize = users['params'].quantity;
        this.isLoaded = true;
      });
  }
  ngAfterViewInit() {}

  sortUsers(options: Sort) {
    let { active, direction } = options;

    direction = direction || 'asc';

    if (active == 'firstName') active = 'first_name';
    if (active == 'lastName') active = 'last_name';
    if (active == 'registrationDate') {
      active = 'registration_date';
      direction = direction == 'asc' ? 'desc' : 'asc';
    }

    this.sortOptions = { active, direction };

    const sortOptions = {
      orderBy: direction,
      sortBy: active,
      limit: this.pageSize,
      offset: this.pageSize * this.currentPage,
    };

    this.usersService.getUsers(sortOptions).subscribe((users) => {
      this.dataSource.data = users['users'];
      this.totalSize = users['params'].quantity;
    });
  }

  handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;

    const options = {
      limit: this.pageSize,
      offset: this.pageSize * this.currentPage,
    };

    this.usersService.getUsers(options).subscribe((users) => {
      this.dataSource.data = users['users'];
    });
  }

  addUser(): void {
    const dialogRef = this.dialog.open(AddUserDialogComponent, {
      width: '300px',
      data: {},
    });

    dialogRef.afterClosed().subscribe((user: IUser) => {
      if (user) {
        this.usersService.createUser(user).subscribe(() => {
          this.sortUsers(this.sortOptions);
        });
      }
    });
  }

  deleteUser(id: number) {
    const dialogRef = this.dialog.open(DeleteUserDialogComponent, {
      width: '300px',
      data: {},
    });

    dialogRef.afterClosed().subscribe((toDelete) => {
      if (toDelete) {
        this.usersService.deleteUser(id).subscribe(() => {
          this.sortUsers(this.sortOptions);
        });
      }
    });
  }

  changeAdminRights(user: IUser) {
    this.usersService.updateUser(user).subscribe();
  }
}
