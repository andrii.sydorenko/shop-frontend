import { Component, Inject, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IUser } from 'src/app/models/user.model';
import { CustomValidators } from 'src/app/utils/custom-validators';
import { AddProductDialogComponent } from '../../products/dialog/add-product-dialog/add-product-dialog.component';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss'],
})
export class AddUserDialogComponent implements OnInit {
  addUserForm: FormGroup = new FormGroup({
    first_name: new FormControl('', [
      CustomValidators.noWhitespaceValidator,
      Validators.required,
      Validators.maxLength(255),
    ]),
    last_name: new FormControl('', [
      CustomValidators.noWhitespaceValidator,
      Validators.required,
      Validators.maxLength(255),
    ]),
    email: new FormControl('', [
      Validators.required,
      Validators.maxLength(70),
      Validators.email,
    ]),
    password: new FormControl('', [
      CustomValidators.noWhitespaceValidator,
      Validators.required,
      Validators.maxLength(255),
      Validators.minLength(6)
    ]),
  });

  users: IUser[] = [];
  selectedPicture: File;

  constructor(
    public dialogRef: MatDialogRef<AddProductDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public user: IUser
  ) {}

  ngOnInit(): void {}

  onCloseClick(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    this.dialogRef.close(this.addUserForm.value);
  }
}
