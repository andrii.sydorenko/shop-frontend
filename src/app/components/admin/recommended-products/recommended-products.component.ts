import { IProduct } from './../../../models/product.model';
import { ProductService } from './../../../services/product.service';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/app/environment';
import { ErrorMessageDialogComponent } from 'src/app/shared/error-message-dialog/error-message-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-recommended-products',
  templateUrl: './recommended-products.component.html',
  styleUrls: ['./recommended-products.component.scss'],
})
export class RecommendedProductsComponent implements OnInit {
  products: IProduct[] = [];
  recommendedProducts: IProduct[] = [];
  isLoaded: boolean = false;
  isRecommendedProductsLoaded: boolean = false;

  url = environment.EDITED_PICTURE_ENDPOINT;
  constructor(
    private productService: ProductService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.getAllRecommendedProducts();
    this.getAllProducts();
  }

  toRecommend(product: IProduct) {
    const { id } = product;

    if (this.recommendedProducts.length < 6) {
      this.productService.addRecommendedProductById(id).subscribe(() => {
        this.products = this.products.filter(
          (product, idx) => product.id !== id
        );
        this.recommendedProducts = [...this.recommendedProducts, product];
      });
    } else {
      this.showError('Max quantity of recommended products is 6 items');
    }
  }

  removeFromRecommendation(product: IProduct) {
    const { id } = product;

    this.products.push(product);
    this.products = this.products.sort((a, b) => a.id - b.id);

    this.productService.deleteRecommendedProduct(product.id).subscribe(() => {
      this.recommendedProducts = this.recommendedProducts.filter(
        (product, idx) => product.id !== id
      );
    });
  }

  private getAllProducts() {
    this.productService
      .getAllProducts({ limit: 100, offset: 0 })
      .subscribe((products) => {
        const ids: number[] = [];

        for (let product of this.recommendedProducts) {
          ids.push(product.id);
        }

        this.products = products['products'].filter((product, idx) => {
          return !ids.includes(product.id);
        });

        this.isLoaded = true;
      });
  }

  private showError(message: string): void {
    const dialogRef = this.dialog.open(ErrorMessageDialogComponent, {
      width: '300px',
      data: message,
    });
  }

  private getAllRecommendedProducts() {
    this.productService.getRecomendedProducts().subscribe((products) => {
      this.recommendedProducts = products;
      this.isRecommendedProductsLoaded = true;
    });
  }
}
