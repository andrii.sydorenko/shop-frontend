import { DeleteProductDialogComponent } from './dialog/delete-product-dialog/delete-product-dialog.component';
import { ICategory } from './../../../models/category.model';
import { IProduct } from './../../../models/product.model';
import { ProductService } from '../../../services/product.service';
import { MatTableDataSource } from '@angular/material/table';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { Sorting } from '../../../utils/sorting';
import { AddProductDialogComponent } from './dialog/add-product-dialog/add-product-dialog.component';
import { environment } from 'src/app/environment';
import { UpdateProductDialogComponent } from './dialog/update-product-dialog/update-product-dialog.component';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit {
  isLoaded: boolean = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  public pageSize = 10;
  public currentPage = 0;
  public totalSize = 0;
  private sortOptions: Sort = { active: 'id', direction: 'asc' };

  displayedColumns: string[] = [
    'id',
    'title',
    'price',
    'description',
    'viewed',
    'creationDate',
    'categoryId',
    'size',
    'picturePath',
    'actions',
  ];
  dataSource: MatTableDataSource<IProduct>;
  url = environment.EDITED_PICTURE_ENDPOINT;

  constructor(
    private productService: ProductService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.productService
      .getAllProducts({
        limit: this.pageSize,
        offset: this.pageSize * this.currentPage,
      })
      .subscribe((products) => {
        this.dataSource = new MatTableDataSource(products['products']);
        this.totalSize = products['params'].quantity;
        this.isLoaded = true;
      });
  }
  ngAfterViewInit() {}

  sortData(sortOptions: Sort) {
    let { active, direction } = sortOptions;

    direction = direction || 'asc';

    if (active == 'creationDate') active = 'creation_date';
    if (active == 'categoryId') active = 'category_id';

    this.sortOptions = { active, direction };

    const options = {
      orderBy: direction,
      sortBy: active,
      limit: this.pageSize,
      offset: this.pageSize * this.currentPage,
    };

    this.productService.getAllProducts(options).subscribe((products) => {
      this.dataSource.data = products['products'];
      this.totalSize = products['params'].quantity;
    });
  }

  addProduct(): void {
    const dialogRef = this.dialog.open(AddProductDialogComponent, {
      width: '300px',
      data: {},
    });
    dialogRef.afterClosed().subscribe((data) => {
      if (data) {
        const { product, picture } = data;
        this.productService.sendProductPicture(picture).subscribe((data) => {
          product.picture_path = data['picture_name'];

          this.productService.createProduct(product).subscribe(() => {
            this.sortData(this.sortOptions);
          });
        });
      }
    });
  }

  editProduct(product: IProduct): void {
    const { id, price, description, title, category_id } = product;
    const dialogRef = this.dialog.open(UpdateProductDialogComponent, {
      width: '300px',
      data: { price, description },
    });
    dialogRef.afterClosed().subscribe((productForm) => {
      if (productForm) {
        const newProduct = { ...productForm, title, category_id };
        this.productService.updateProduct(id, newProduct).subscribe(() => {
          this.sortData(this.sortOptions);
        });
      }
    });
  }

  deleteProduct(id) {
    const dialogRef = this.dialog.open(DeleteProductDialogComponent, {
      width: '300px',
      data: {},
    });
    dialogRef.afterClosed().subscribe((toDelete) => {
      if (toDelete) {
        this.productService.deleteProduct(id).subscribe(() => {
          this.sortData(this.sortOptions);
        });
      }
    });
  }

  handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;

    const options = {
      limit: this.pageSize,
      offset: this.pageSize * this.currentPage,
    };

    this.productService.getAllProducts(options).subscribe((products) => {
      this.dataSource.data = products['products'];
      this.totalSize = products['params'].quantity;
    });
  }
}
