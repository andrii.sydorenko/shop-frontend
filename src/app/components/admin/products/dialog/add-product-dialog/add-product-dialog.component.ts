import { CategoryService } from './../../../../../services/category.service';
import { ICategory } from './../../../../../models/category.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IProduct } from './../../../../../models/product.model';
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CustomValidators } from 'src/app/utils/custom-validators';

@Component({
  selector: 'app-add-product-dialog',
  templateUrl: './add-product-dialog.component.html',
  styleUrls: ['./add-product-dialog.component.scss'],
})
export class AddProductDialogComponent implements OnInit {
  addProductForm: FormGroup = new FormGroup({
    title: new FormControl('', [
      CustomValidators.noWhitespaceValidator,
      Validators.required,
      Validators.maxLength(255),
    ]),
    price: new FormControl('', [
      Validators.required,
      Validators.pattern(/^\d{1,5}(\.\d{1,2})?$/),
    ]),
    description: new FormControl(''),
    category_id: new FormControl('', Validators.required),
    size: new FormControl('', [
      Validators.required,
      Validators.pattern('^[0-9]+$'),
      Validators.min(28),
      Validators.max(49)
    ]),
  });

  categories: ICategory[] = [];
  selectedPicture: File;

  constructor(
    public dialogRef: MatDialogRef<AddProductDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public product: IProduct,
    private categoryService: CategoryService
  ) {}

  @Output() productEvent = new EventEmitter();

  ngOnInit(): void {
    this.categoryService
      .getCategories({ limit: 1000, offset: 0 })
      .subscribe((categories) => (this.categories = categories['categories']));
  }

  onCloseClick(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    this.dialogRef.close({
      product: this.addProductForm.value,
      picture: this.onUpload(),
    });
  }

  onPictureChanged(event) {
    this.selectedPicture = event.target.files[0];
  }

  onUpload(): FormData {
    const uploadData = new FormData();
    uploadData.append(
      'picture',
      this.selectedPicture,
      this.selectedPicture.name
    );
    
    return uploadData;
  }
}
