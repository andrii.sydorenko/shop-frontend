import { IProduct } from './../../../../../models/product.model';
import { ICategory } from './../../../../../models/category.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-update-product-dialog',
  templateUrl: './update-product-dialog.component.html',
  styleUrls: ['./update-product-dialog.component.scss'],
})
export class UpdateProductDialogComponent implements OnInit {
  updateProductForm: FormGroup = new FormGroup({
    price: new FormControl('', [
      Validators.required,
      Validators.pattern(/^\d{1,5}(\.\d{1,2})?$/),
    ]),
    description: new FormControl(''),
  });
  constructor(
    public dialogRef: MatDialogRef<UpdateProductDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public product: IProduct
  ) {}

  ngOnInit(): void {
    this.updateProductForm.setValue(this.product);    
  }

  onCloseClick(): void {
    this.dialogRef.close();
  }

  onUpdateClick(): void {
    this.dialogRef.close(this.updateProductForm.value)
  }
}
