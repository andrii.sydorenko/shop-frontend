import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { DeleteDialogComponent } from '../../../categories/dialog/delete-dialog/delete-dialog.component';

@Component({
  selector: 'app-delete-product-dialog',
  templateUrl: './delete-product-dialog.component.html',
  styleUrls: ['./delete-product-dialog.component.scss'],
})
export class DeleteProductDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DeleteDialogComponent>) {}

  ngOnInit(): void {}

  onCloseClick() {
    this.dialogRef.close();
  }
}
