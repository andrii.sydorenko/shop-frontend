import { FormControl, Validators } from '@angular/forms';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ICategory } from 'src/app/models/category.model';
import { CustomValidators } from 'src/app/utils/custom-validators';

@Component({
  selector: 'app-update-dialog',
  templateUrl: './update-dialog.component.html',
  styleUrls: ['./update-dialog.component.scss'],
})
export class UpdateDialogComponent implements OnInit {
  updateCategory: FormControl = new FormControl('', [
    CustomValidators.noWhitespaceValidator,
    Validators.required,
    Validators.maxLength(70),
  ]);
  constructor(
    public dialogRef: MatDialogRef<UpdateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public category: ICategory
  ) {}

  ngOnInit(): void {
    this.updateCategory.setValue(this.category.category_name)
  }

  onCloseClick(): void {
    this.dialogRef.close();
  }
}
