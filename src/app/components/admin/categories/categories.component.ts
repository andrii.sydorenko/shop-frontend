import { Sort } from '@angular/material/sort';
import { DeleteDialogComponent } from './dialog/delete-dialog/delete-dialog.component';
import { ICategory } from '../../../models/category.model';
import { CategoryService } from '../../../services/category.service';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { AddDialogComponent } from './dialog/add-dialog/add-dialog.component';
import { UpdateDialogComponent } from './dialog/update-dialog/update-dialog.component';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
})
export class CategoriesComponent implements OnInit, AfterViewInit {
  isLoaded: boolean = false;

  public pageSize = 10;
  public currentPage = 0;
  public totalSize = 0;
  private sortOptions: Sort = { active: 'id', direction: 'asc' };

  displayedColumns: string[] = ['id', 'categoryName', 'actions'];
  dataSource: MatTableDataSource<ICategory>;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private categoryService: CategoryService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.categoryService
      .getCategories({
        limit: this.pageSize,
        offset: this.pageSize * this.currentPage,
      })
      .subscribe((categories) => {
        this.dataSource = new MatTableDataSource(categories['categories']);
        this.totalSize = categories['params'].quantity;
        this.isLoaded = true;
      });
  }
  ngAfterViewInit() {}

  sortData(sortOptions: Sort) {
    let { active, direction } = sortOptions;

    direction = direction || 'asc';

    if (active == 'categoryName') active = 'category_name';

    this.sortOptions = { active, direction };

    const options = {
      orderBy: direction,
      sortBy: active,
      limit: this.pageSize,
      offset: this.pageSize * this.currentPage,
    };

    this.categoryService.getCategories(options).subscribe((categories) => {
      this.dataSource.data = categories['categories'];
      this.totalSize = categories['params'].quantity;
    });
  }

  addCategory(): void {
    const dialogRef = this.dialog.open(AddDialogComponent, {
      width: '300px',
      data: {},
    });

    dialogRef.afterClosed().subscribe((categoryName) => {
      if (categoryName) {
        this.categoryService
          .createCategory({ category_name: categoryName })
          .subscribe(() => {
            this.sortData(this.sortOptions);
          });
      }
    });
  }

  editCategory(category: ICategory): void {
    const { id, category_name } = category;
    const dialogRef = this.dialog.open(UpdateDialogComponent, {
      width: '300px',
      data: { id, category_name },
    });

    dialogRef.afterClosed().subscribe((categoryName) => {
      if (categoryName) {
        this.categoryService.updateCategory(id, categoryName).subscribe(() => {
          this.sortData(this.sortOptions);
        });
      }
    });
  }

  deleteCategory(id) {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '300px',
      data: {},
    });

    dialogRef.afterClosed().subscribe((toDelete) => {
      if (toDelete) {
        this.categoryService.deleteCategory(id).subscribe(() => {
          this.sortData(this.sortOptions);
        });
      }
    });
  }

  handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;

    const options = {
      limit: this.pageSize,
      offset: this.pageSize * this.currentPage,
    };

    this.categoryService.getCategories(options).subscribe((categories) => {
      this.dataSource.data = categories['categories'];
      this.totalSize = categories['params'].quantity;
    });
  }
}
