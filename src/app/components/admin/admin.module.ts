import { MatIconModule } from '@angular/material/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSortModule } from '@angular/material/sort';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { ProductsComponent } from './products/products.component';
import { CategoriesComponent } from './categories/categories.component';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { AddDialogComponent } from './categories/dialog/add-dialog/add-dialog.component';
import { DeleteDialogComponent } from './categories/dialog/delete-dialog/delete-dialog.component';
import { UpdateDialogComponent } from './categories/dialog/update-dialog/update-dialog.component';
import { UpdateProductDialogComponent } from './products/dialog/update-product-dialog/update-product-dialog.component';
import { DeleteProductDialogComponent } from './products/dialog/delete-product-dialog/delete-product-dialog.component';
import { AddProductDialogComponent } from './products/dialog/add-product-dialog/add-product-dialog.component';
import { MatSelectModule } from '@angular/material/select';
import { RecommendedProductsComponent } from './recommended-products/recommended-products.component';
import { UsersComponent } from './users/users.component';
import { AddUserDialogComponent } from './users/add-user/add-user.component';
import { DeleteUserDialogComponent } from './users/delete-user/delete-user.component';

@NgModule({
  declarations: [
    AdminComponent,
    ProductsComponent,
    CategoriesComponent,
    AddDialogComponent,
    DeleteDialogComponent,
    UpdateDialogComponent,
    UpdateProductDialogComponent,
    DeleteProductDialogComponent,
    AddProductDialogComponent,
    RecommendedProductsComponent,
    UsersComponent,
    AddUserDialogComponent,
    DeleteUserDialogComponent,
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatSelectModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatDialogModule,
    MatSortModule,
    FormsModule,
    ReactiveFormsModule,
    MatCheckboxModule
  ],
})
export class AdminModule {}
