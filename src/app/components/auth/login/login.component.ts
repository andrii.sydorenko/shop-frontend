import { MatDialog } from '@angular/material/dialog';
import { CartService } from './../../../services/cart.service';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { CustomValidators } from 'src/app/utils/custom-validators';
import { ErrorMessageDialogComponent } from 'src/app/shared/error-message-dialog/error-message-dialog.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm = this.formBuilder.group({
    email: [
      '',
      [
        CustomValidators.noWhitespaceValidator,
        CustomValidators.email,
        Validators.required,
        Validators.maxLength(70),
      ],
    ],
    password: [
      '',
      [
        CustomValidators.noWhitespaceValidator,
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(200),
      ],
    ],
  });

  isLoggedIn: boolean = false;
  isAdmin: boolean;
  isLoginFailed: boolean = false;
  errorMessage: string = '';

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private tokenStorage: TokenStorageService,
    private cartService: CartService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
    }
  }

  onSubmit(): void {
    if (this.loginForm.valid) {
      this.authService.login(this.loginForm.value).subscribe(
        (data) => {
          const { accessToken, shoppingCart, isAdmin } = data;
          this.tokenStorage.saveToken(accessToken);

          this.tokenStorage.saveShoppingCart(shoppingCart.shopping_cart_id);
          this.tokenStorage.saveUser(this.loginForm.controls.email.value);
          this.tokenStorage.saveAdmin(isAdmin);
          this.isLoginFailed = false;
          
          this.cartService.syncData(shoppingCart.shopping_cart_id).then(() => {
            this.reloadPage();
            this.isLoggedIn = true;
          });
        },
        (err) => {
          this.errorMessage = err.error.message;
          this.isLoginFailed = true;

          this.showError(err.error.message);
        }
      );
    }
  }

  showError(message: string): void {
    const dialogRef = this.dialog.open(ErrorMessageDialogComponent, {
      width: '300px',
      data: message,
    });
  }

  private reloadPage(): void {
    window.location.assign('/');
  }
}
