import { CartService } from './../../../services/cart.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { CustomValidators } from 'src/app/utils/custom-validators';
import { MatDialog } from '@angular/material/dialog';
import { ErrorMessageDialogComponent } from 'src/app/shared/error-message-dialog/error-message-dialog.component';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit {
  signupForm = new FormBuilder().group({
    firstName: [
      '',
      [
        CustomValidators.noWhitespaceValidator,
        Validators.required,
        Validators.maxLength(255),
      ],
    ],
    lastName: [
      '',
      [
        CustomValidators.noWhitespaceValidator,
        Validators.required,
        Validators.maxLength(255),
      ],
    ],
    email: [
      '',
      [
        CustomValidators.noWhitespaceValidator,
        Validators.required,
        CustomValidators.email,
        Validators.maxLength(70),
      ],
    ],
    password: [
      '',
      [
        CustomValidators.noWhitespaceValidator,
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(200),
      ],
    ],
  });
  isSuccessful = false;
  isSignUpFailed = false;

  constructor(
    private authService: AuthService,
    private tokenStorage: TokenStorageService,
    private cartService: CartService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {}

  onSubmit(): void {
    if (this.signupForm.valid) {
      this.authService.register(this.signupForm.value).subscribe(
        (data) => {
          this.isSuccessful = true;
          this.isSignUpFailed = false;

          this.login(this.signupForm.value);
        },
        (err) => {
          this.isSignUpFailed = true;
          this.showError(err.error.message);
        }
      );
    }
  }

  private login(credentials) {
    const { email, password } = credentials;
    this.authService.login({ email, password }).subscribe((data) => {
      const { accessToken, shoppingCart, isAdmin } = data;
      this.tokenStorage.saveToken(accessToken);
      this.tokenStorage.saveShoppingCart(shoppingCart.shopping_cart_id);
      this.tokenStorage.saveUser(email);
      this.tokenStorage.saveAdmin(isAdmin);

      this.cartService.syncData(shoppingCart.shopping_cart_id).then(() => {
        this.reloadPage();
      });
    });
  }

  showError(message: string): void {
    const dialogRef = this.dialog.open(ErrorMessageDialogComponent, {
      width: '300px',
      data: message,
    });
  }

  private reloadPage(): void {
    window.location.assign('/');
  }
}
