import { ICartItem } from './../../../models/cart.model';
import { CartService } from './../../../services/cart.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { IProduct } from './../../../models/product.model';
import { ProductService } from './../../../services/product.service';
import { FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/app/environment';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {
  products: IProduct[] = [];
  isSynchronized: boolean = false;
  userEmail: string;
  isLoaded: boolean = false;
  total: number = 0;
  url = environment.EDITED_PICTURE_ENDPOINT;

  quantity: FormControl;

  constructor(
    private tokenStorage: TokenStorageService,
    private productService: ProductService,
    private cartService: CartService
  ) {}

  ngOnInit(): void {
    this.userEmail = this.tokenStorage.getUser();
    this.getProducts();
  }

  onQuantityChange(product: IProduct, quantity: number) {
    const { id } = product;

    if (quantity < 1 || quantity > 10000) {
      quantity = 1;
    }

    const newCartItem: ICartItem = { product_id: id, quantity: +quantity };

    if (this.userEmail) {
      this.cartService.updateCartItem(newCartItem).subscribe((cartItem) => {
        this.getTotal();
      });
    }

    this.tokenStorage.changeQuantity(id, quantity);

    for (let product of this.products) {
      if (product.id === id) {
        product.quantity = quantity;
      }
    }

    if (!this.userEmail) {
      let total = 0;
      for (let product of this.products) {
        total += product.price * product.quantity;
      }
      this.total = total;
    }
  }

  incrementQuantity(product) {
    this.onQuantityChange(product, product.quantity + 1);
  }
  decrementQuantity(product) {
    this.onQuantityChange(product, product.quantity - 1);
  }

  removeProduct(id: number) {
    this.tokenStorage.removeCartItem(id);

    if (this.userEmail) {
      this.cartService.removeCartItem(id).subscribe(() => {
        this.getTotal();
      });
    }

    this.products = this.products.filter((product, idx) => product.id !== id);

    if (!this.userEmail) {
      let total = 0;
      for (let product of this.products) {
        total += product.price * product.quantity;
      }
      this.total = total;
    }
  }

  private async getProducts() {
    let cartItems = await this.tokenStorage.getCartItems();

    for (let cartItem of cartItems) {
      this.productService
        .getProduct(cartItem.product_id)
        .subscribe((product) => {
          if (product) {
            product.quantity = cartItem.quantity;
            this.products.push(product);
          }
        });
    }

    await this.getTotal();
    this.isLoaded = true;
  }

  async getTotal() {
    if (this.userEmail) {
      this.cartService.getTotal().subscribe((total) => {
        this.total = total['total'];
      });
    } else {
      this.tokenStorage.getCartItems().forEach((cartItem, idx) => {
        this.productService
          .getProduct(cartItem.product_id)
          .subscribe((product) => {
            this.total += product.price * cartItem.quantity;
          });
      });
    }
  }

  clearCart() {
    this.tokenStorage.clearCart();

    if (this.userEmail) {
      this.cartService.clearCart().subscribe();
    }
    this.total = 0;
    this.products = [];
  }

  onPurchase() {
    alert(`You payed $${this.total}!!!`);
    this.clearCart();
  }
}
