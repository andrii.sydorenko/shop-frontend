import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ICategory } from 'src/app/models/category.model';
import { CategoryService } from 'src/app/services/category.service';
import { ProductService } from 'src/app/services/product.service';
import { environment } from 'src/app/environment';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  url = environment.EDITED_PICTURE_ENDPOINT
  categories: ICategory[] = [];


  constructor(
    private productService: ProductService,
    private categoryService: CategoryService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe((queryParams) =>
      this.searchProducts(queryParams.text)
    );
  }

  searchProducts(searchInput: string) {
    const filter = (searchInput || '').trim();

    if (!filter) {
      this.categories = [];
    }

    if (filter) {
      this.productService
        .getFilteredProducts(filter, { limit: 5, offset: 0 })
        .subscribe((products) => {
          let prevCategoryId: number;
          const categoryProducts = [];

          for (let product of products) {
            if (product.category_id !== prevCategoryId) {
              this.categoryService
                .getCategoryById(product.category_id)
                .subscribe((category) => {
                  const productCategory = products.filter(
                    (product, idx) => product.category_id === category.id
                  );
                  category.products = productCategory;
                  categoryProducts.push(category);
                });
              prevCategoryId = product.category_id;
            }
          }
          this.categories = categoryProducts;          
        });
    }
  }

  updateProducts(searchInput: string) {
    if (searchInput === '' && this.categories.length) {
      this.categories = [];
    }
  }
}
