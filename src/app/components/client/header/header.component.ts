import { Component, OnInit } from '@angular/core';

import { TokenStorageService } from 'src/app/services/token-storage.service';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  isLoggedIn: boolean = false;
  isAdmin: boolean = false;

  searchInput: FormControl = new FormControl();

  constructor(
    private tokenStorage: TokenStorageService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorage.getToken();
    this.isAdmin = this.tokenStorage.getAdmin();
  }

  logout(): void {
    this.tokenStorage.signOut();
    window.location.reload();
  }

  openSearchPage() {
    this.router.navigate(['search'], {
      queryParams: { text: this.searchInput.value },
    });
  }
}
