import { ProductService } from '../../../services/product.service';
import { IProduct } from '../../../models/product.model';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { FormControl } from '@angular/forms';
import { environment } from 'src/app/environment';

@Component({
  selector: 'app-product-category',
  templateUrl: './product-category.component.html',
  styleUrls: ['./product-category.component.scss'],
})
export class ProductCategoryComponent implements OnInit, AfterViewInit {
  products: IProduct[] = [];
  url = environment.SERVER_ENDPOINT + 'product-images-edited/';

  minPrice: FormControl = new FormControl('');
  maxPrice: FormControl = new FormControl('');
  sortOptions: FormControl;

  public pageSize = 10;
  public currentPage = 0;
  public totalSize = 0;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private route: ActivatedRoute,
    private productService: ProductService
  ) {}

  ngOnInit(): void {
    this.getProducts();
    this.getPrices();
    this.sortOptions = new FormControl('');
  }

  ngAfterViewInit(): void {}

  async getProducts() {
    const categoryId = +this.route.snapshot.paramMap.get('id');
    const options = {
      limit: this.pageSize,
      offset: this.currentPage * this.pageSize,
    };

    await this.productService
      .getProductsByCategory(categoryId, options)
      .subscribe((data) => {
        const { products, params } = data;

        this.products = products;

        this.totalSize = params.quantity;
      });
  }

  getPrices() {
    const categoryId = +this.route.snapshot.paramMap.get('id');

    this.getMinPrice(categoryId);
    this.getMaxPrice(categoryId);
  }

  handlePage(e: any) {
    const categoryId = +this.route.snapshot.paramMap.get('id');
    const priceRange = { min: this.minPrice.value, max: this.maxPrice.value };

    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;

    const options = {
      ...this.sortOptions.value,
      limit: this.pageSize,
      offset: this.pageSize * this.currentPage,
    };

    this.productService
      .getProductsFilteredByPriceAndOrdered(priceRange, options, categoryId)
      .subscribe((products) => (this.products = products['products']));
  }

  filterProducts(sortOptions) {
    const categoryId = +this.route.snapshot.paramMap.get('id');

    const min = this.minPrice.value;
    const max = this.maxPrice.value;
    const priceRange = { min, max };

    const options = {
      ...this.sortOptions.value,
      limit: this.pageSize,
      offset: this.pageSize * this.currentPage,
    };

    this.productService
      .getProductsFilteredByPriceAndOrdered(priceRange, options, categoryId)
      .subscribe((products) => {
        this.products = products['products'];
        this.totalSize = products['params'].countProducts;
      });
  }

  sortProducts() {
    this.filterProducts(this.sortOptions.value);
  }

  private getMinPrice(categoryId: number) {
    this.productService
      .getProductsByCategory(categoryId, {
        limit: 1,
        offset: 0,
        orderBy: 'asc',
        sortBy: 'price',
      })
      .subscribe((products) => {
        if (products.length) {
          this.minPrice.setValue(products['products'][0].price);
        }
      });
  }

  private getMaxPrice(categoryId: number) {
    this.productService
      .getProductsByCategory(categoryId, {
        limit: 1,
        offset: 0,
        orderBy: 'desc',
        sortBy: 'price',
      })
      .subscribe((products) => {
        if (products.length) {
          this.maxPrice.setValue(products['products'][0].price);
        }
      });
  }
}
