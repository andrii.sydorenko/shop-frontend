import { ICartItem } from './../../../models/cart.model';
import { MatDialog } from '@angular/material/dialog';
import { FeedbackService } from './../../../services/feedback.service';
import { IFeedback } from './../../../models/feedback.model';
import { FormControl, Validators } from '@angular/forms';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { IProduct } from '../../../models/product.model';
import { ProductService } from '../../../services/product.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/app/environment';
import { CartService } from 'src/app/services/cart.service';
import { ISize } from 'src/app/models/size.model';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
})
export class ProductComponent implements OnInit {
  product: IProduct;
  feedbacks: IFeedback[] = [];
  loadCompleted: boolean = false;
  url = environment.SERVER_ENDPOINT;
  pictureEndpoint = environment.PICTURE_ENDPOINT;
  isLoggedIn: boolean = false;
  productId: number;
  userEmail: string;
  isNoSize: boolean = false;
  sizes: Array<ISize> = [];

  feedbackMessage: FormControl;

  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
    private tokenStorage: TokenStorageService,
    private feedbackService: FeedbackService,
    private router: Router,
    private cartService: CartService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.getProduct();
    this.getFeedbacks();
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.feedbackMessage = new FormControl('', [Validators.maxLength(255)]);
      this.userEmail = this.tokenStorage.getUser();
    }
  }

  getProduct() {
    this.productId = +this.route.snapshot.paramMap.get('id');

    this.productService.getProduct(this.productId).subscribe((product) => {
      this.product = product;

      this.getSizes(this.product.title);

      this.loadCompleted = true;
    });
  }

  getSizes(title: string) {
    this.productService.getSizesByTitle(title).subscribe((sizes) => {
      this.sizes = sizes;
    });
  }

  addProduct(productId: number) {
    const cartItem: ICartItem = { product_id: productId, quantity: 1 };

    if (this.userEmail) {
      this.cartService.postCartItem(cartItem).subscribe(() => {
        this.tokenStorage.saveCartItem(cartItem);
      });
    }

    if (!this.userEmail) {
      this.tokenStorage.saveCartItem(cartItem);
    }
  }

  isAdded() {
    return this.tokenStorage.isProductAdded(this.product.id);
  }

  getFeedbacks() {
    const productId = +this.route.snapshot.paramMap.get('id');

    this.feedbackService
      .getAllFeedbacksByProduct(productId)
      .subscribe((feedbacks) => {
        this.feedbacks = feedbacks;
      });
  }

  sendFeedback() {
    const feedbackMessage = (this.feedbackMessage.value || '').trim();

    if (feedbackMessage) {
      const feedback: IFeedback = {
        product_id: this.productId,
        user_email: this.userEmail,
        feedback_message: feedbackMessage,
      };
      this.feedbackService.createFeedback(feedback).subscribe((feedback) => {
          this.feedbacks = [feedback, ...this.feedbacks];
      });
      this.feedbackMessage.reset();
    }
  }

  async switchSize(id: number) {
    await this.router.navigate(['/product/' + id]);
    this.getProduct();
  }
}
