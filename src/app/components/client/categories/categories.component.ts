import { CategoryService } from '../../../services/category.service';
import { Component, OnInit } from '@angular/core';
import { ICategory } from '../../../models/category.model';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
})
export class CategoriesComponent implements OnInit {
  categories: ICategory[] = [];
  active: number;
  constructor(private categoryService: CategoryService) {}

  ngOnInit(): void {
    this.categoryService
      .getCategories({limit: 100, offset: 0})
      .subscribe((categories) => this.categories = categories['categories']);
  }
}
