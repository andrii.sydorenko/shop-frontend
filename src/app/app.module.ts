import { AuthGuardService } from './services/auth-guard.service';
import { MatIconModule } from '@angular/material/icon';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/client/header/header.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MainComponent } from './components/client/main/main.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CategoriesComponent } from './components/client/categories/categories.component';
import { AuthModule } from './components/auth/auth.module';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RecomendedProductsComponent } from './pages/recomended-products/recomended-products.component';
import { RecentlyViewedProductsComponent } from './pages/recently-viewed-products/recently-viewed-products.component';
import { RecentlyAddedProductsComponent } from './pages/recently-added-products/recently-added-products.component';
import { PopularProductsComponent } from './pages/popular-products/popular-products.component';
import { ProductComponent } from './components/client/product/product.component';
import { ProductCategoryComponent } from './components/client/product-category/product-category.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { CartComponent } from './components/client/cart/cart.component';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatBadgeModule } from '@angular/material/badge';
import {MatTabsModule} from '@angular/material/tabs';
import { BannerComponent } from './pages/banner/banner.component';
import { ProductCardComponent } from './shared/product-card/product-card.component';
import { ErrorMessageDialogComponent } from './shared/error-message-dialog/error-message-dialog.component';
import { SearchComponent } from './components/client/search/search.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    CategoriesComponent,
    RecomendedProductsComponent,
    RecentlyViewedProductsComponent,
    RecentlyAddedProductsComponent,
    PopularProductsComponent,
    ProductComponent,
    ProductCategoryComponent,
    CategoriesComponent,
    CartComponent,
    BannerComponent,
    ProductCardComponent,
    ErrorMessageDialogComponent,
    SearchComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    MatIconModule,
    MatInputModule,
    MatPaginatorModule,
    NoopAnimationsModule,
    AuthModule,
    FormsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatCardModule,
    MatButtonModule,
    MatListModule,
    MatBadgeModule,
    BrowserAnimationsModule,
    MatTabsModule,
  ],
  providers: [AuthGuardService],
  bootstrap: [AppComponent],
})
export class AppModule {}
