export const environment = {
  production: true,
  SERVER_ENDPOINT: 'http://dcodeit.net:20000/',
  PICTURE_ENDPOINT: 'http://dcodeit.net:20000/product-images/',
  EDITED_PICTURE_ENDPOINT: 'http://dcodeit.net:20000/product-images-edited/',
};
